﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    class ProjectContext:DbContext
    {
        
        public DbSet<Joyeria> joyerias { get; set; }
        public DbSet<Mueble> muebles { get; set; }
        public DbSet<Comida> comidas { get; set; }
        public DbSet<Ropa> ropas { get; set; }
        public DbSet<Electronica> electronicas { get; set; }
        public DbSet<Material> materiales { get; set; }
        public DbSet<Funcionalidad> funcionalidades { get; set; }
    }
}
