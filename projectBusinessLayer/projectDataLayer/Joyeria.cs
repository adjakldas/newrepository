﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    class Joyeria:Articulo
    {
        public Material material { get; set; }
        public decimal kilates { get; set; }

        public Joyeria()
        {
            base.categoria = "Joyeria";
        }
    }
}
