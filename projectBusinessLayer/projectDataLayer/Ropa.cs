﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    class Ropa:Articulo
    {
        public string talle { get; set; }
        public Material material { get; set; }

        public Ropa()
        {
            base.categoria = "Ropa";
        }
        
    }
}
