﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    public abstract class Articulo
    {
        public int articuloId { get; set; }
        public string nombre { get; set; }
        public string marca { get; set; }
        public string descripcion { get; set; }
        public int antiguedad { get; set; }
        public string estado { get; set; }
        public int puntaje { get; set; }
        public byte[] imagen { get; set; }
        public string categoria { get; set; }
    }
}
