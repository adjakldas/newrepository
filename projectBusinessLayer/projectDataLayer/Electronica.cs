﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    class Electronica:Articulo
    {
        public int capacidad { get; set; }
        public Funcionalidad funcionalidad { get; set; } 

        public Electronica()
        {
            base.categoria = "Electronica";
        }
    }
}
