﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    class Mueble:Articulo
    {
        public Material material { get; set; }
        public decimal largo { get; set; }
        public decimal ancho { get; set; }
        public decimal alto { get; set; }

        public Mueble() {
            base.categoria = "Mueble";
        }
       
    }
}
