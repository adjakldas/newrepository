﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectDataLayer
{
    public class Comida:Articulo
    {
        public string vencimiento { get; set; }
        public int cantidad { get; set; }
        public Boolean casero { get; set; }


        public Comida()
        {
            base.categoria = "Comida";
        }
    }
}
